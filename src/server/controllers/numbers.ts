import { Request, Response, Router } from "express";

const router: Router = Router();

router.get("/", (req: Request, res: Response) => {
  const { random, floor } = Math;
  res.send({
    number: floor(random() * 100),
  });
});

export const NumbersController: Router = router;
