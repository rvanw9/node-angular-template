import express from "express";
import { NumbersController } from "./controllers";

const app: express.Application = express();

const port = process.env.PORT || 3000;

app.use("/numbers", NumbersController);

app.listen(port, () => {
  console.log(`Listening at http://localhost:${port}/`);
});
